        .data

prompt:       .asciiz  "input: "
outmsg:       .asciiz  "output:\n"
newline:      .asciiz  "\n"
nopalin:      .asciiz  "No palindrome found\n"
input:        .space 100  # char input_sentence[100];
word:         .space 20   # char word[20];

        .text

downcase:  # char downcase(char ch)

        sge $t4, $t2, 'A'
        sle $s4, $t2, 'Z'
        bne $t4, $s4, already_lowercase

        addi $t2, 32

already_lowercase:

        jr $ra

is_delimiting_char:         # int split_char(char ch)

        beq $t2, ' ',  next
        beq $t2, ',',  next
        beq $t2, '.',  next
        beq $t2, '!',  next
        beq $t2, '?',  next
        beq $t2, '_',  next
        beq $t2, '-',  next
        beq $t2, '(',  next
        beq $t2, ')',  next
        beq $t2, '\n', next

        li $v0, 0
        jr $ra

next:

        li $v0, 4
        la $a0, newline
        syscall

        addi $t1, $t1, 1
        j loop


        .globl main

main:

        li $v0, 4       # print_string("input: ");
        la $a0, prompt
        syscall

        li $v0, 8       # current_char = read_char();
        la $a0, input
        li $a1, 100
        move $s0, $a0
        syscall

        li $v0, 4       # print_string("output:\n");
        la $a0, outmsg
        syscall

        li $t0, 0 # char_index = 0;
        li $t1, 0 # loop counter, k

loop:

        add $t3, $s0, $t1 # $t3 = sentence[$t0]
        lb $t2, 0($t3)    # $t2 = *$t3?
        beqz $t2, end     # $t2 != '\0'

        jal is_delimiting_char

        beqz $v0, print_word # if (delimiting_char)

        addi $t1, $t1, 1

        j loop

print_word:

        li $v0, 11
        move $a0, $t2
        syscall

        addi $t1, $t1, 1

        j loop

        #la $a0, newline
        #syscall

end:

        li $v0, 10
        syscall
