// ==========================================================================
// Palindrome Finder
// ==========================================================================
// Prints the palindromes in an input sentence.
//
// Inf2C-CS Coursework 1. Task B
// OUTLINE code, to be completed as part of coursework.

// Boris Grot, Rakesh Kumar
//  12 Oct 2015

//---------------------------------------------------------------------------
// C definitions for SPIM system calls
//---------------------------------------------------------------------------
#include <stdio.h>

int read_char() { return getchar(); }
void read_string(char* s, int size) { fgets(s, size, stdin); }

void print_char(int c)     { putchar(c); }
void print_string(char* s) { printf("%s", s); }

//---------------------------------------------------------------------------
// Functions
//---------------------------------------------------------------------------

int split_char(char ch)
{
  switch (ch) {
    case ' ':
    case ',':
    case '.':
    case '!':
    case '?':
    case '_':
    case '-':
    case '(':
    case ')':
    case '\n':
      return 1;
      break;
    default:
      return 0;
  }
}

char downcase(char ch)
{
  if (ch >= 'A' && ch <= 'Z')
    ch += 32;

  return ch;
}

int is_palindrome(char* word, int word_length)
{
  if (word_length - 2 < 2) return 0;

  for (int i = 0; i < word_length; ++i) {
    for (int j = (word_length - 2); j > i; --j) {
      if (downcase(word[i]) != downcase(word[j])) {
        return 0;
      }
      return 1;
    }
  }
}

//---------------------------------------------------------------------------
// MAIN function
//---------------------------------------------------------------------------


int main (int argc, char** argv) {

  char input_sentence[100];
  int i = 0, j, k;
  char current_char;

  char word[20];
  int char_index, delimiting_char;

  /////////////////////////////////////////////



  /////////////////////////////////////////////

  /* Infinite loop
   * Asks for input sentence and prints the palindromes in it
   * Terminated by user (e.g. CTRL+C)
   */

  while(1) {

    i = 0;

    print_char('\n');

    print_string("input: ");

    /* Read the input sentence.
     * It is just a sequence of character terminated by a new line (\n) character.
     */

    do {
      current_char = read_char();
      input_sentence[i] = current_char;
      i++;
    } while (current_char != '\n');

    /////////////////////////////////////////////


    print_string("output:\n");
    char_index = 0;

    int palindrome_counter = 0;

    // From word_finder.c

    for (k = 0; k < i; k++)  {
      current_char = input_sentence[k];
      delimiting_char = split_char(current_char);

      if (delimiting_char) {
        if (char_index > 0) {     // Avoids printing a blank line in case of consecutive delimiting characters.
          word[char_index++] = '\n';    // Puts an newline character so the next word in printed in a new line.

          if (is_palindrome(word, char_index)) {
            for (int i = 0; i < char_index; ++i) {
              print_char(word[i]);
            }
            /* print_string(word); */
            palindrome_counter++;
          }

          char_index = 0;
        }
      }
      else {
        word[char_index++] = current_char;
      }
    }

    if (palindrome_counter == 0) {
      print_string("No palindrome found\n");
    }
    /////////////////////////////////////////////

  }

  return 0;
}


//---------------------------------------------------------------------------
// End of file
//---------------------------------------------------------------------------
