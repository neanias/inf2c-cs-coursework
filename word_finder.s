        .data

prompt:       .asciiz  "input: "
outmsg:       .asciiz  "output:\n"
newline:      .asciiz  "\n"
input:        .space 100  # char input_sentence[100];
word:         .space 20   # char word[20];

        .text

is_delimiting_char:   # int is_delimiting_char(char ch)

        beq $t2, ' ',  next # if (ch == ' ') return 1
        beq $t2, ',',  next # else if (ch == ',') return 1
        beq $t2, '.',  next # else if (ch == '.') return 1
        beq $t2, '!',  next # else if (ch == '!') return 1
        beq $t2, '?',  next # else if (ch == '?') return 1
        beq $t2, '_',  next # else if (ch == '_') return 1
        beq $t2, '-',  next # else if (ch == '-') return 1
        beq $t2, '(',  next # else if (ch == '(') return 1
        beq $t2, ')',  next # else if (ch == ')') return 1
        beq $t2, '\n', next # else if (ch == '\n') return 1

        li $v0, 0 # else return 0;
        jr $ra    # jump back to where called from

next:

        li $v0, 4        # print a newline
        la $a0, newline
        syscall

        addi $t1, $t1, 1 # increase loop counter
        j loop


        .globl main

main:

        li $v0, 4       # print_string("input: ");
        la $a0, prompt
        syscall

        li $v0, 8       # current_char = read_char();
        la $a0, input
        li $a1, 100
        move $s0, $a0
        syscall

        li $v0, 4       # print_string("output:\n");
        la $a0, outmsg
        syscall

        li $t0, 0 # char_index = 0;
        li $t1, 0 # loop counter, k

loop:

        add $t3, $s0, $t1      # $t3 = sentence[$t0]
        lb $t2, 0($t3)         # $t2 = *$t3?
        beqz $t2, end          # $t2 != '\0'

        jal is_delimiting_char # call is_delimiting_char

        beqz $v0, print_word   # if (delimiting_char)

        addi $t1, $t1, 1       # k++

        j loop

print_word:

        li $v0, 11       # print_char word[j]
        move $a0, $t2
        syscall

        addi $t1, $t1, 1 # k++

        j loop

        #la $a0, newline
        #syscall

end:

        li $v0, 10  # exit
        syscall
